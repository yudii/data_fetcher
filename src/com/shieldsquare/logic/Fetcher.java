package com.shieldsquare.logic;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import redis.clients.jedis.Jedis;

public class Fetcher {

	public void fetch() throws ParseException {

		Jedis jedis = new Jedis("169.45.1.145", 6379);
		jedis.auth("j~36wgjPkm(L)tY");

		/*
		 * Jedis jedis = new Jedis("104.239.157.67", 6379);
		 * jedis.auth("redis@123@Azure");
		 */

	//	System.out.println(jedis.ping());
		while (true) {

			// List<String> a = jedis.brpop(h);
			// List<String> b= jedis.brpop("L:masterbkupjs");

			// String api = jedis.rpop("L:masterbkupapi");
			try {

				String api = jedis.rpop("L:masterbkupapi");
			//	System.out.println(api);

				// String js = jedis.rpop("L:masterbkupjs");
				JSONParser m = new JSONParser();
				if (!api.isEmpty()) {

					JSONObject jsonObject = (JSONObject) m.parse(api);
				//	System.out.println(jsonObject.keySet());

					if (jsonObject.keySet().contains("isBot"))
						;
					jsonObject.remove("isBot");
					if (jsonObject.keySet().contains("r1"))
						;
					jsonObject.remove("r1");
					if (jsonObject.keySet().contains("r2"))
						;
					jsonObject.remove("r2");
					if (jsonObject.keySet().contains("r4"))
						;
					jsonObject.remove("r4");
					if (jsonObject.keySet().contains("r5"))
						;
					jsonObject.remove("r5");

					if (jsonObject.keySet().contains("ssresp"))
						;
					jsonObject.remove("ssresp");

			//		System.out.println("heloooo" + jsonObject);

					// DataOutputStream out = null;
					try {
						String url1 = "104.239.157.67";
						sync_sendReq2SS(url1, jsonObject.toString());
					} catch (Exception e) {
						System.out.println(e);
					}

				} else {

					System.out.println("empty");
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	public void sync_sendReq2SS(String ss2url, String apidata) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			ObjectNode ss2respJson = mapper.createObjectNode();
			HttpURLConnection connection = null;
			DataOutputStream out = null;
			BufferedInputStream inStream = null;
			URL url = new URL("http://" + ss2url + "/getRequestData");
			connection = (HttpURLConnection) url.openConnection();

			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-type", "application/json");
			connection.setRequestProperty("Content-Encoding", "UTF-8");
			out = new DataOutputStream(connection.getOutputStream());
			out.writeBytes(URLEncoder.encode(apidata, "UTF-8"));

			out.flush();
			connection.connect();
			inStream = new BufferedInputStream(connection.getInputStream());

			int statusCode = connection.getResponseCode();
			if (statusCode != 200) {
				inStream = new BufferedInputStream(connection.getErrorStream());
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		Fetcher f = new Fetcher();
		f.fetch();
	}

}